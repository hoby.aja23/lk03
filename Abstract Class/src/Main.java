/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * 
 * "Abstract class lebih cocok digunakan dalam konteks kalkulator jika 
 * memiliki hierarki class dengan beberapa implementasi yang mirip, tetapi 
 * memiliki perilaku yang sedikit berbeda. Abstract class dapat memberikan 
 * implementasi dasar dan fleksibilitas bagi subclass untuk mengimplementasikan perilaku spesifik yang berbeda." 
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}